package ru.lab5;

import org.apache.commons.configuration.DataConfiguration;
import org.junit.Assert;
import org.junit.Test;
import ru.lab4.client.ProjectClient;
import ru.lab4.dto.Dto;
import ru.lab4.endpoint.IProjectEndpoint;
import ru.lab4.model.Project;

import java.util.Date;

import static org.junit.Assert.*;

public class ProjectClientTest {

    @Test
    public void findById() {
        ProjectClient client = new ProjectClient();
        Project p = client.findById("0d31e3c3-b718-4b4c-bc3e-b64165595b18");
        Assert.assertNotNull(p);
    }

    @Test
    public void save() {
        ProjectClient client = new ProjectClient();
        Project p = new Project();
        p.setName("TEST CLIENT");
        Project saveResult = client.save(p);
        Assert.assertNotNull(saveResult);
    }

    @Test
    public void getDate(){
        ProjectClient client = new ProjectClient();
        Dto<Date> dto = client.getDate();
        Assert.assertNotNull(dto.getValue());
    }

    @Test
    public void getDateWithFeign(){
        IProjectEndpoint client = IProjectEndpoint.client("http://localhost:8080/api/project");
        Dto<Date> dto = client.getDate();
        Assert.assertNotNull(dto.getValue());
    }
}