package ru.lab4.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.lab4.dto.Dto;
import ru.lab4.model.Project;
import ru.lab4.service.ProjectService;

import java.util.Date;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/project")
public class ProjectEndpoint implements IProjectEndpoint {

    private final ProjectService projectService;

    public ProjectEndpoint(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    @GetMapping("/{projectId}")
    public Project findById(
            @PathVariable("projectId") String id
    ) {
        return projectService.findById(id).orElse(null);
    }

    @Override
    @DeleteMapping("/{projectId}")
    public void deleteById(
            @PathVariable("projectId") String id
    ) {
        projectService.deleteById(id);
    }

    @Override
    @GetMapping("/all")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @PostMapping(value = "/",consumes = APPLICATION_JSON_VALUE)
    public Project save(
            @RequestBody Project s
    ) {
        return projectService.save(s);
    }

    @Override
    @GetMapping(value = "/date",produces = APPLICATION_JSON_VALUE)
    public Dto<Date> getDate() {
        return new Dto<>(new Date());
    }

}
