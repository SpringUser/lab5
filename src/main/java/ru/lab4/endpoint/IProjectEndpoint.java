package ru.lab4.endpoint;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.lab4.dto.Dto;
import ru.lab4.model.Project;

import java.util.Date;
import java.util.List;

public interface IProjectEndpoint {

    static IProjectEndpoint client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectEndpoint.class, baseUrl);
    }

    @GetMapping("/{projectId}")
    Project findById(
            @PathVariable("projectId") String id
    );

    @DeleteMapping("/{projectId}")
    void deleteById(
            @PathVariable("projectId") String id
    );

    @GetMapping("/all")
    List<Project> findAll();

    @PostMapping("/")
    Project save(
            @RequestBody() Project s
    );

    @GetMapping("/date")
    Dto<Date> getDate();
}
