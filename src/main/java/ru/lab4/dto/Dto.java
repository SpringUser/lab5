package ru.lab4.dto;

public class Dto<T> {

    public Dto() {
    }

    public Dto(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    private T value;


}
