package ru.lab4.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.lab4.model.Project;
import ru.lab4.repository.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepo;

    public List<Project> findAll() {
        return projectRepo.findAll();
    }

    @Transactional
    public <S extends Project> S save(S s) {
        return projectRepo.save(s);
    }

    public Optional<Project> findById(String s) {
        return projectRepo.findById(s);
    }

    @Transactional
    public void deleteById(String s) {
        projectRepo.deleteById(s);
    }
}
