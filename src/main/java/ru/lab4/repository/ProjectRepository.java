package ru.lab4.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.lab4.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {
}
