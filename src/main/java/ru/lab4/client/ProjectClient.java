package ru.lab4.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import ru.lab4.dto.Dto;
import ru.lab4.endpoint.IProjectEndpoint;
import ru.lab4.model.Project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ProjectClient implements IProjectEndpoint {

    @Override
    public Project findById(String id) {
        final RestTemplate template = new RestTemplate();
        return template.getForObject("http://localhost:8080/api/project/" + id, Project.class);
    }

    @Override
    public void deleteById(String id) {
        final RestTemplate template = new RestTemplate();
        template.delete("http://localhost:8080/api/project/" + id);
    }

    @Override
    public List<Project> findAll() {
        return null;
    }

    @Override
    public Project save(Project s) {
        final String url = "http://localhost:8080/api/project/";
        final RestTemplate template = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity entity = new HttpEntity(s, headers);
        return template.postForObject(url, entity, Project.class);
    }

    @Override
    public Dto<Date> getDate() {
        final RestTemplate template = new RestTemplate();
        final List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        template.setMessageConverters(messageConverters);
        return template.getForObject("http://localhost:8080/api/project/date", Dto.class);
    }


}
